﻿using FY.Retaguarda.Application;
using FY.Retaguarda.Application.Interfaces;
using FY.Retaguarda.Domain.Interfaces.Repositories;
using FY.Retaguarda.Domain.Interfaces.Services;
using FY.Retaguarda.Domain.Services;
using FY.Retaguarda.Infra.Data.Context;
using FY.Retaguarda.Infra.Data.Context.Interface;
using FY.Retaguarda.Infra.Data.Repositories;

using SimpleInjector;
using SimpleInjector.Diagnostics;

namespace FY.Retaguarda.Infra.DI
{
    public class SetupDI
    {
        public static Container RegisterComponents(Container container)
        {
            //Contexto
            container.Register<IFYContext, FYContext>(Lifestyle.Scoped);

            //Repository
            container.Register<IClienteRepository, ClienteRepository>();

            //Service
            container.Register<IClienteService, ClienteService>();

            //Aplication
            container.Register<IClienteAppService, ClienteAppService>();

            Registration registration = container.GetRegistration(typeof(IFYContext)).Registration;

            registration.SuppressDiagnosticWarning(DiagnosticType.DisposableTransientComponent,
                "Reason of suppression");

            container.Verify();

            return container;

        }
    }
}

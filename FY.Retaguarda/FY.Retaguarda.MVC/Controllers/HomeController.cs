﻿using FY.Retaguarda.Application;
using FY.Retaguarda.Application.Interfaces;
using FY.Retaguarda.Domain.Entities;
using FY.Retaguarda.Domain.Interfaces.Repositories;
using FY.Retaguarda.Domain.Interfaces.Services;
using FY.Retaguarda.Domain.Services;
using FY.Retaguarda.Infra.Data.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FY.Retaguarda.MVC.Controllers
{
    public class HomeController : Controller
    {
        private readonly IEmpresaAppService _empresaApp;
        private readonly IEmpresaService _empresaService;
        private readonly IEmpresaRepository _empresaRepository;

        public HomeController()
        {
            //_empresaRepository = new EmpresaRepository();
            //_empresaService = new EmpresaService(_empresaRepository);
            //_empresaApp = new EmpresaAppService(_empresaService);
        }

        public ActionResult Index()
        {
            var empresas = _empresaApp.GetAll().ToList();
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}
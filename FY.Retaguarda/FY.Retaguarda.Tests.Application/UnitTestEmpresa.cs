﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FY.Retaguarda.Application;
using FY.Retaguarda.Application.Interfaces;
using FY.Retaguarda.Domain.Interfaces.Services;
using FY.Retaguarda.Domain.Services;
using FY.Retaguarda.Domain.Interfaces.Repositories;
using FY.Retaguarda.Domain.Entities;
using System.Linq;

namespace FY.Retaguarda.Tests.Application
{
    [TestClass]
    public class UnitTestEmpresa
    {
        private readonly IEmpresaAppService _empresaApp;
        private readonly IEmpresaService _empresaService;
        private readonly IEmpresaRepository _empresaRepository;

        private readonly IClienteAppService _clienteApp;
        private readonly IClienteService _clienteService;
        private readonly IClienteRepository _clienteRepository;

        public UnitTestEmpresa()
        {
            _empresaRepository = null;
            _empresaService = new EmpresaService(_empresaRepository);
            _empresaApp = new EmpresaAppService(_empresaService);

            _clienteRepository = null;
            _clienteService = new ClienteService(_clienteRepository);
            _clienteApp = new ClienteAppService(_clienteService);
        }

        [TestMethod]
        public void TesteGetAll()
        {
            var empresas = _empresaApp.GetAll().ToList();
            Assert.IsTrue(empresas != null);
        }

        [TestMethod]
        public void TesteInsertEmpresaSemDadosRegraPontuacao()
        {
            Empresa e = new Empresa()
            {
                Nome = "Fidelize You",
                CNPJ = "12585311000149",
            };
            var empresa = _empresaApp.Add(e);

            Assert.IsTrue(empresa.EmpresaId > 0);
        }

        [TestMethod]
        public void TesteInsertEmpresaComDadosRegraPontuacao()
        {
            Empresa e = new Empresa()
            {
                Nome = "Fidelize You",
                CNPJ = "12585311000149",
                Mensagem = "Mensagem para pontuação",
                Valor = 10,
                Pontos = 10
            };
            var empresa = _empresaApp.Add(e);

            Assert.IsTrue(empresa.EmpresaId > 0);
        }

        [TestMethod]
        public void TesteGetById()
        {
            var e1 = _empresaApp.GetAll().FirstOrDefault();
            int id = e1.EmpresaId;
            var empresa = _empresaApp.GetById(id);
            Assert.IsTrue(empresa.EmpresaId > 0 && empresa.EmpresaId == id);
        }

        [TestMethod]
        public void TesteUpdate()
        {
            var e1 = _empresaApp.GetAll().FirstOrDefault();
            int id = e1.EmpresaId;
            string nome = "Fidelize You 2";

            var empresa = _empresaApp.GetById(id);
            empresa.Nome = nome;

            var e = _empresaApp.Update(empresa);

            Assert.IsTrue(e.Nome == nome && e.EmpresaId == id);
        }

        [TestMethod]
        public void TesteUpdateRegraPontuacao()
        {
            var e = _empresaApp.GetAll().FirstOrDefault();
            var id = e.EmpresaId;
            e.Valor = Math.PI * e.EmpresaId;
            e.Pontos = Math.PI * e.EmpresaId;
            e.Mensagem = "Mensagem para a empresa: " + e.Nome;            
            var emp = _empresaApp.Update(e);
            Assert.IsTrue(emp.EmpresaId == id);
        }

        [TestMethod]
        public void TestRemove()
        {
            var e1 = _empresaApp.GetAll().LastOrDefault();
            int id = e1.EmpresaId;
            var empresa = _empresaApp.GetById(id);
            _empresaApp.Remove(empresa);
            var e = _empresaApp.GetById(id);
            Assert.IsTrue(e == null);
        }

        [TestMethod]
        public void TesteRemoveEmpresaComClientes()
        {
            //try
            //{
            //    var e1 = _empresaApp.GetAll();
            //    foreach (var e in e1)
            //    {
            //        var c = _clienteApp.BuscarClientesPorEmpresa(e.EmpresaId);
            //        if (c.Any())
            //        {
            //            _empresaApp.Remove(e);
            //            Assert.IsFalse(true);
            //        }
            //    }
            //}
            //catch(Exception ex)
            //{
            //    Assert.IsTrue(true);
            //}
            
        }

        [TestMethod]
        public void TesteGetAllBest()
        {
            try
            {
                var empresas = _empresaApp.GetAll();

                Assert.IsTrue(empresas.Any() || !empresas.Any());
            }
            catch (Exception)
            {
                Assert.IsTrue(false);
            }
            
        }
    }
}

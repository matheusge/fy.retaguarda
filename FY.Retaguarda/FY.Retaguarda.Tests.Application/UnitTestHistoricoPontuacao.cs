﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using FY.Retaguarda.Application.Interfaces;
using FY.Retaguarda.Domain.Interfaces.Services;
using FY.Retaguarda.Domain.Interfaces.Repositories;
using FY.Retaguarda.Infra.Data.Repositories;
using FY.Retaguarda.Domain.Services;
using FY.Retaguarda.Application;
using System.Linq;
using FY.Retaguarda.Domain.Entities;

namespace FY.Retaguarda.Tests.Application
{
    [TestClass]
    public class UnitTestHistoricoPontuacao
    {

        private readonly IClienteAppService _clienteApp;
        private readonly IClienteService _clienteService;
        private readonly IClienteRepository _clienteRepository;

        private readonly IHistoricoPontuacaoAppService _historicoPontuacaoApp;
        private readonly IHistoricoPontuacaoService _historicoPontuacaoService;
        private readonly IHistoricoPontuacaoRepository _historicoPontuacaoRepository;

        public UnitTestHistoricoPontuacao()
        {
            _clienteRepository = null;
            _clienteService = new ClienteService(_clienteRepository);
            _clienteApp = new ClienteAppService(_clienteService);

            _historicoPontuacaoRepository = null;
            _historicoPontuacaoService = new HistoricoPontuacaoService(_historicoPontuacaoRepository);
            _historicoPontuacaoApp = new HistoricoPontuacaoAppService(_historicoPontuacaoService);
        }

        [TestMethod]
        public void TesteInserindoHistorico()
        {
            var c = _clienteApp.GetAll().FirstOrDefault();

            var h = new HistoricoPontuacao();
            h.ClienteId = c.ClienteId;
            h.Pontos = 10;

            _historicoPontuacaoApp.Add(h);

            Assert.IsTrue(h.HistoricoPontuacaoId > 0);
        }
    }
}

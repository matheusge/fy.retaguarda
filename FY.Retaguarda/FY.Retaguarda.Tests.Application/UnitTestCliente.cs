﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FY.Retaguarda.Application.Interfaces;
using FY.Retaguarda.Domain.Interfaces.Services;
using FY.Retaguarda.Domain.Interfaces.Repositories;
using FY.Retaguarda.Infra.Data.Repositories;
using FY.Retaguarda.Domain.Services;
using FY.Retaguarda.Application;
using FY.Retaguarda.Domain.Entities;
using System.Linq;

namespace FY.Retaguarda.Tests.Application
{
    /// <summary>
    /// Summary description for UnitTestCliente
    /// </summary>
    [TestClass]
    public class UnitTestCliente
    {

        private readonly IClienteAppService _clienteApp;
        private readonly IClienteService _clienteService;
        private readonly IClienteRepository _clienteRepository;

        private readonly IEmpresaAppService _empresaApp;
        private readonly IEmpresaService _empresaService;
        private readonly IEmpresaRepository _empresaRepository;

        public UnitTestCliente()
        {
            _clienteRepository = null;
            _clienteService = new ClienteService(_clienteRepository);
            _clienteApp = new ClienteAppService(_clienteService);

            //_empresaRepository = new EmpresaRepository();
            //_empresaService = new EmpresaService(_empresaRepository);
            //_empresaApp = new EmpresaAppService(_empresaService);
        }

        [TestMethod]
        public void InsertClienteSemCPF()
        {
            try
            {
                Cliente c = new Cliente()
                {
                    Nome = "Cliente Fidelize You"
                };
                var cl = _clienteApp.Add(c);
                Assert.IsFalse(cl.ClienteId > 0);
            }
            catch(Exception e)
            {
                Assert.IsTrue(true);
            }
        }

        [TestMethod]
        public void InsertClienteSemEmpresa()
        {
            try
            {
                Cliente c = new Cliente()
                {
                    Nome = "Cliente Fidelize You",
                    CPF = "0123456789"
                };
                var cl = _clienteApp.Add(c);
                Assert.IsFalse(cl.ClienteId > 0);
            }
            catch (Exception e)
            {
                Assert.IsTrue(true);
            }
        }

        [TestMethod]
        public void InsertClienteComEmpresa()
        {
            var e = _empresaApp.GetAll().FirstOrDefault();

            Cliente c = new Cliente()
            {
                Nome = "Cliente Fidelize You",
                CPF = "0123456789",
                EmpresaId = e.EmpresaId
            };

            var cli = _clienteApp.Add(c);
            Assert.IsTrue(cli.ClienteId > 0);
        }

        [TestMethod]
        public void TesteSomarPontosNegativo()
        {
            try
            {
                var c = _clienteApp.GetAll().FirstOrDefault();
                c.SomarPontos(-1);
                Assert.IsTrue(false);
            }catch(Exception e)
            {
                Assert.IsTrue(true);
            }
            
        }

        [TestMethod]
        public void TesteSomarPontos()
        {
            var cliente = _clienteApp.GetAll().FirstOrDefault();
            var c = _clienteApp.GetById(cliente.ClienteId);

            double totalPontos = cliente.TotalPontos;
            double somarPontos = 10;
            c.SomarPontos(somarPontos);

            c = _clienteApp.Update(c);
            var result = double.Equals(c.TotalPontos, (totalPontos + somarPontos));
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void TesteNaoSomarPontosComValorNegativo()
        {
            try
            {
                var cliente = _clienteApp.GetAll().FirstOrDefault();
                var c = _clienteApp.GetById(cliente.ClienteId);

                double totalPontos = cliente.TotalPontos;
                double somarPontos = -10;
                c.SomarPontos(somarPontos);

                c = _clienteApp.Update(c);
                var result = double.Equals(c.TotalPontos, (totalPontos + somarPontos));
                Assert.IsTrue(result);
            }
            catch(Exception ex)
            {
                Assert.IsTrue(true);
            }
        }

        [TestMethod]
        public void TesteSubtrairPontos()
        {
            var cliente = _clienteApp.GetAll().FirstOrDefault();
            var c = _clienteApp.GetById(cliente.ClienteId);

            double totalPontos = cliente.TotalPontos;
            double subtrairPontos = 10;
            c.SubtrairPontos(subtrairPontos);

            c = _clienteApp.Update(c);
            var result = double.Equals(c.TotalPontos, (totalPontos - subtrairPontos));
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void TesteNaoSubtrairPontosComValorNegativo()
        {
            try
            {
                var cliente = _clienteApp.GetAll().FirstOrDefault();
                var c = _clienteApp.GetById(cliente.ClienteId);

                double totalPontos = cliente.TotalPontos;
                double subtrairPontos = -10;
                c.SubtrairPontos(subtrairPontos);

                c = _clienteApp.Update(c);
                var result = double.Equals(c.TotalPontos, (totalPontos - subtrairPontos));
                Assert.IsTrue(false);
            }
            catch (Exception ex)
            {
                Assert.IsTrue(true);
            }
        }
    }
}

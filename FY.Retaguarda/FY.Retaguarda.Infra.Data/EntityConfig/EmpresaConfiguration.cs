﻿using System.Data.Entity.ModelConfiguration;
using FY.Retaguarda.Domain.Entities;
using System.ComponentModel.DataAnnotations.Schema;

namespace FY.Retaguarda.Infra.Data.EntityConfig
{
    public class EmpresaConfiguration : EntityTypeConfiguration<Empresa>
    {
        public EmpresaConfiguration()
        {
            HasKey(x => x.EmpresaId);

            Property(p => p.EmpresaId).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            Property(x => x.Nome)
                .IsRequired()
                .HasMaxLength(150);

            Property(x => x.CNPJ)
                .IsRequired()
                .HasMaxLength(14);

            Property(x => x.Mensagem)
                .HasMaxLength(250);
        }
    }
}

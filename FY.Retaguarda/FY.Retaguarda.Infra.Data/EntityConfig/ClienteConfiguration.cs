﻿
using FY.Retaguarda.Domain.Entities;
using System.Data.Entity.ModelConfiguration;

namespace FY.Retaguarda.Infra.Data.EntityConfig
{
    public class ClienteConfiguration : EntityTypeConfiguration<Cliente>
    {
        public ClienteConfiguration()
        {
            HasKey(x => x.ClienteId);

            Property(x => x.Nome)
                .IsRequired()
                .HasMaxLength(150);

            Property(x => x.CPF)
                .IsRequired()
                .HasMaxLength(14);

            Property(x => x.Celular)
                .HasMaxLength(15);

            Property(x => x.Email)
                .HasMaxLength(150);

            HasRequired(p => p.Empresa)
                .WithMany()
                .HasForeignKey(p => p.EmpresaId);
        }
    }
}

﻿using FY.Retaguarda.Domain.Entities;
using System.Data.Entity.ModelConfiguration;

namespace FY.Retaguarda.Infra.Data.EntityConfig
{
    public class HistoricoPontuacaoConfiguration : EntityTypeConfiguration<HistoricoPontuacao>
    {
        public HistoricoPontuacaoConfiguration()
        {
            HasKey(x => x.HistoricoPontuacaoId);

            Property(x => x.Pontos)
                .IsRequired();

            HasRequired(x => x.Cliente)
               .WithMany()
               .HasForeignKey(x => x.ClienteId);
        }
        
    }
}

namespace FY.Retaguarda.Infra.Data.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using FY.Retaguarda.Infra.Data.Context;

    internal sealed class Configuration : DbMigrationsConfiguration<FY.Retaguarda.Infra.Data.Context.FYContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            SetSqlGenerator("MySql.Data.MySqlClient", new MySql.Data.Entity.MySqlMigrationSqlGenerator());
        }

        protected override void Seed(FYContext context)
        {
        }
    }
}

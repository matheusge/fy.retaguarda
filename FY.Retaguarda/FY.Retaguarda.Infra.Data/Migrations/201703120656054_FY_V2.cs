namespace FY.Retaguarda.Infra.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FY_V2 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Cliente", "CPF", c => c.String(nullable: false, maxLength: 14, unicode: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Cliente", "CPF", c => c.String(nullable: false, maxLength: 11, unicode: false));
        }
    }
}

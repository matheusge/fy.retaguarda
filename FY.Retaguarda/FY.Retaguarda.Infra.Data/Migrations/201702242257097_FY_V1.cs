namespace FY.Retaguarda.Infra.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FY_V1 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Cliente",
                c => new
                    {
                        ClienteId = c.Int(nullable: false, identity: true),
                        Nome = c.String(nullable: false, maxLength: 150, unicode: false),
                        CPF = c.String(nullable: false, maxLength: 14, unicode: false),
                        Genero = c.Int(nullable: false),
                        DataNascimento = c.DateTime(nullable: false, precision: 0),
                        Celular = c.String(maxLength: 15, unicode: false),
                        Email = c.String(maxLength: 150, unicode: false),
                        DataCadastro = c.DateTime(nullable: false, precision: 0),
                        TotalPontos = c.Double(nullable: false),
                        EmpresaId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ClienteId)
                .ForeignKey("dbo.Empresa", t => t.EmpresaId)
                .Index(t => t.EmpresaId);
            
            CreateTable(
                "dbo.Empresa",
                c => new
                    {
                        EmpresaId = c.Int(nullable: false, identity: true),
                        Nome = c.String(nullable: false, maxLength: 150, unicode: false),
                        CNPJ = c.String(nullable: false, maxLength: 14, unicode: false),
                        Valor = c.Double(),
                        Pontos = c.Double(),
                        Mensagem = c.String(maxLength: 250, unicode: false),
                        DataCadastro = c.DateTime(nullable: false, precision: 0),
                    })
                .PrimaryKey(t => t.EmpresaId);
            
            CreateTable(
                "dbo.HistoricoPontuacao",
                c => new
                    {
                        HistoricoPontuacaoId = c.Int(nullable: false, identity: true),
                        DataCadastro = c.DateTime(nullable: false, precision: 0),
                        Pontos = c.Double(nullable: false),
                        ClienteId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.HistoricoPontuacaoId)
                .ForeignKey("dbo.Cliente", t => t.ClienteId)
                .Index(t => t.ClienteId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.HistoricoPontuacao", "ClienteId", "dbo.Cliente");
            DropForeignKey("dbo.Cliente", "EmpresaId", "dbo.Empresa");
            DropIndex("dbo.HistoricoPontuacao", new[] { "ClienteId" });
            DropIndex("dbo.Cliente", new[] { "EmpresaId" });
            DropTable("dbo.HistoricoPontuacao");
            DropTable("dbo.Empresa");
            DropTable("dbo.Cliente");
        }
    }
}

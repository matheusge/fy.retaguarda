﻿using System.Data.Entity;
using FY.Retaguarda.Domain.Entities;
using System.Data.Entity.Infrastructure;

namespace FY.Retaguarda.Infra.Data.Context.Interface
{
    public interface IFYContext
    {
        DbSet<Cliente> Cliente { get; set; }
        DbSet<Empresa> Empresa { get; set; }
        DbSet<HistoricoPontuacao> HistoricoPontuacao { get; set; }

        DbSet<TEntity> Set<TEntity>() where TEntity : class;
        DbEntityEntry<TEntity> Entry<TEntity>(TEntity entity) where TEntity : class;

        int SaveChanges();
    }
}
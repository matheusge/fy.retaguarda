﻿using FY.Retaguarda.Domain.Entities;
using FY.Retaguarda.Domain.Interfaces.Repositories;
using FY.Retaguarda.Infra.Data.Context.Interface;
using System.Collections.Generic;
using System.Linq;

namespace FY.Retaguarda.Infra.Data.Repositories
{
    public class ClienteRepository : RepositoryBase<Cliente>, IClienteRepository
    {
        public ClienteRepository(IFYContext context)
            : base(context)
        {
        }

        public IEnumerable<Cliente> BuscarClientesPorEmpresa(int empresaId)
        {
            return base._context.Cliente.Where(x => x.EmpresaId == empresaId);
        }
    }
}

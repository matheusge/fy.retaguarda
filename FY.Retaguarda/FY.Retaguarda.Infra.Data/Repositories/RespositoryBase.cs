﻿using FY.Retaguarda.Domain.Interfaces.Repositories;
using FY.Retaguarda.Infra.Data.Context.Interface;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace FY.Retaguarda.Infra.Data.Repositories
{
    public abstract class RepositoryBase<TEntity> : IRepositoryBase<TEntity> where TEntity : class
    {
        protected readonly IFYContext _context = null;
        protected RepositoryBase(IFYContext context)
        {
            _context = context;
        }

        public TEntity Add(TEntity obj)
        {
            _context.Set<TEntity>().Add(obj);
            _context.SaveChanges();
            return obj;
        }

        public TEntity GetById(int id)
        {
            return _context.Set<TEntity>().Find(id);
        }

        public IEnumerable<TEntity> GetAll()
        {
            return _context.Set<TEntity>().ToList();
        }

        public TEntity Update(TEntity obj)
        {
            var name = string.Concat(typeof(TEntity).Name, "Id");
            var id = obj.GetType().GetProperty(name).GetValue(obj, null);
            var entity = _context.Set<TEntity>().Find(id);

            _context.Entry(entity).State = EntityState.Modified;
            _context.Entry(entity).CurrentValues.SetValues(obj);

            _context.SaveChanges();
            return obj;
        }

        public void Remove(TEntity obj)
        {
            var id = obj.GetType().GetProperty(string.Concat(obj.GetType().Name, "Id")).GetValue(obj, null);
            var entity = _context.Set<TEntity>().Find(id);

            _context.Entry(entity).State = EntityState.Deleted;
            _context.Set<TEntity>().Remove(entity);
            _context.SaveChanges();
        }

    }
}

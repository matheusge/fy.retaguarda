﻿using FY.Retaguarda.Domain.Entities;
using FY.Retaguarda.Domain.Interfaces.Repositories;
using FY.Retaguarda.Infra.Data.Context.Interface;

namespace FY.Retaguarda.Infra.Data.Repositories
{
    public class HistoricoPontuacaoRepository : RepositoryBase<HistoricoPontuacao>, IHistoricoPontuacaoRepository
    {
        public HistoricoPontuacaoRepository(IFYContext context)
            : base(context)
        {
        }
    }
}

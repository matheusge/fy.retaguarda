﻿using FY.Retaguarda.Domain.Entities;
using FY.Retaguarda.Domain.Interfaces.Repositories;
using FY.Retaguarda.Infra.Data.Context.Interface;

namespace FY.Retaguarda.Infra.Data.Repositories
{
    public class EmpresaRepository : RepositoryBase<Empresa>, IEmpresaRepository
    {
        public EmpresaRepository(IFYContext context)
            : base(context)
        {
        }
    }
}

﻿using System;

namespace FY.Retaguarda.Domain.Entities
{
    public class HistoricoPontuacao
    {
        public int HistoricoPontuacaoId { get; set; }
        public DateTime DataCadastro { get; set; }
        public double Pontos { get; set; }
        public int ClienteId { get; set; }
        public virtual Cliente Cliente { get; set; }
    }
}

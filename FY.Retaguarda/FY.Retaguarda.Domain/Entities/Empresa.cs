﻿using System;
using System.Collections.Generic;

namespace FY.Retaguarda.Domain.Entities
{
    public class Empresa
    {
        public int EmpresaId { get; set; }
        public string Nome { get; set; }
        public string CNPJ { get; set; }
        public double? Valor { get; set; }
        public double? Pontos { get; set; }
        public string Mensagem { get; set; }
        public DateTime DataCadastro { get; set; }

        public virtual IEnumerable<Cliente> Clientes { get; set; }
    }
}

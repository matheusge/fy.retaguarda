﻿using FY.Retaguarda.Domain.ObjectsValue;
using System;
using System.Collections.Generic;

namespace FY.Retaguarda.Domain.Entities
{
    public class Cliente
    {
        public Cliente()
        {
            Genero = EnumGenero.SemGenero;
        }

        public int ClienteId { get; set; }
        public string Nome { get; set; }
        public string CPF { get; set; }
        public EnumGenero Genero { get; set; }
        public DateTime DataNascimento { get; set; }
        public string Celular { get; set; }
        public string Email { get; set; }
        public DateTime DataCadastro { get; set; }
        public double TotalPontos { get; set; }
        public int EmpresaId { get; set; }
        public virtual Empresa Empresa { get; set; }

        public virtual ICollection<HistoricoPontuacao> HistoricoPontuacao { get; set; }

        /// <summary>
        /// Método para somar os pontos na propriedade TotalPontos
        /// </summary>
        /// <param name="p"></param>
        public void SomarPontos(double p)
        {
            if (p < 0)
                throw new Exception("O valor deve ser positivo e maior que zero");

            TotalPontos += p;
        }

        /// <summary>
        /// Método para subtrair os pontos na propriedade TotalPontos
        /// </summary>
        /// <param name="p"></param>
        public void SubtrairPontos(double p)
        {
            if (p < 0)
                throw new Exception("O valor deve ser positivo e maior que zero");

            if (TotalPontos == 0)
                throw new Exception(string.Format("O cliente {0} não possúi pontos para resgate", Nome));

            if ((TotalPontos - p) < 0)
                throw new Exception("Quantidade de pontos para resgate é maior que total acumulado");

            TotalPontos -= p;
        }

        /// <summary>
        /// Copia as informações do objeto recebido
        /// </summary>
        /// <param name="c"></param>
        public void PreencherDados(Cliente c)
        {
            this.ClienteId = c.ClienteId;
            this.Nome = c.Nome;
            this.CPF = c.CPF;
            this.Genero = c.Genero;
            this.DataNascimento = c.DataNascimento;
            this.Celular = c.Celular;
            this.Email = c.Email;
            this.DataCadastro = c.DataCadastro;
            this.TotalPontos = c.TotalPontos;
            this.EmpresaId = c.EmpresaId;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FY.Retaguarda.Domain.ObjectsValue
{
    public enum EnumGenero
    {
        Masculino,
        Feminino,
        SemGenero
    }
}

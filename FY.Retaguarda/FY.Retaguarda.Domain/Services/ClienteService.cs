﻿using System.Collections.Generic;
using FY.Retaguarda.Domain.Entities;
using FY.Retaguarda.Domain.Interfaces.Repositories;
using FY.Retaguarda.Domain.Interfaces.Services;

namespace FY.Retaguarda.Domain.Services
{
    public class ClienteService : ServiceBase<Cliente>, IClienteService
    {
        private readonly IClienteRepository _clienteRepository;
        public ClienteService(IClienteRepository clienteRepository)
            : base(clienteRepository)
        {
            _clienteRepository = clienteRepository;
        }

        public IEnumerable<Cliente> BuscarClientesPorEmpresa(int empresaId)
        {
            return _clienteRepository.BuscarClientesPorEmpresa(empresaId);
        }
    }
}

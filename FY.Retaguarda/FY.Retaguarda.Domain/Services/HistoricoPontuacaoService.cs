﻿using FY.Retaguarda.Domain.Entities;
using FY.Retaguarda.Domain.Interfaces.Repositories;
using FY.Retaguarda.Domain.Interfaces.Services;

namespace FY.Retaguarda.Domain.Services
{
    public class HistoricoPontuacaoService : ServiceBase<HistoricoPontuacao>, IHistoricoPontuacaoService
    {
        private readonly IHistoricoPontuacaoRepository _historicoPontuacaoRepository;
        public HistoricoPontuacaoService(IHistoricoPontuacaoRepository historicoPontuacaoRepository)
            : base(historicoPontuacaoRepository)
        {
            _historicoPontuacaoRepository = historicoPontuacaoRepository;
        }
    }
}

﻿using FY.Retaguarda.Domain.Entities;
using FY.Retaguarda.Domain.Interfaces.Repositories;
using FY.Retaguarda.Domain.Interfaces.Services;

namespace FY.Retaguarda.Domain.Services
{
    public class EmpresaService : ServiceBase<Empresa>, IEmpresaService
    {
        private readonly IEmpresaRepository _empresaRepository;
        public EmpresaService(IEmpresaRepository empresaRepository)
            : base(empresaRepository)
        {
            _empresaRepository = empresaRepository;
        }
    }
}

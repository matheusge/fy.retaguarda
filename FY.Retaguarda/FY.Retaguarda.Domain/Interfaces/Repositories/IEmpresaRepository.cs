﻿using FY.Retaguarda.Domain.Entities;

namespace FY.Retaguarda.Domain.Interfaces.Repositories
{
    public interface IEmpresaRepository : IRepositoryBase<Empresa>
    {
    }
}

﻿using FY.Retaguarda.Domain.Entities;
using System.Collections.Generic;

namespace FY.Retaguarda.Domain.Interfaces.Repositories
{
    public interface IClienteRepository : IRepositoryBase<Cliente>
    {
        IEnumerable<Cliente> BuscarClientesPorEmpresa(int empresaId);
    }
}

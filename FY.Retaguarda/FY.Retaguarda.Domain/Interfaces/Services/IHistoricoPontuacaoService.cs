﻿using FY.Retaguarda.Domain.Entities;

namespace FY.Retaguarda.Domain.Interfaces.Services
{
    public interface IHistoricoPontuacaoService : IServiceBase<HistoricoPontuacao>
    {
    }
}

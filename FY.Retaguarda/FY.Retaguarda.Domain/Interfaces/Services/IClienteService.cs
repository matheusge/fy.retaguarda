﻿using FY.Retaguarda.Domain.Entities;
using System.Collections.Generic;

namespace FY.Retaguarda.Domain.Interfaces.Services
{
    public interface IClienteService : IServiceBase<Cliente>
    {
        IEnumerable<Cliente> BuscarClientesPorEmpresa(int empresaId);
    }
}

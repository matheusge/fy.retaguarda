﻿using FY.Retaguarda.Application.Interfaces;
using FY.Retaguarda.Domain.Entities;
using FY.Retaguarda.Domain.Interfaces.Services;

namespace FY.Retaguarda.Application
{
    public class EmpresaAppService : AppServiceBase<Empresa>, IEmpresaAppService
    {
        private readonly  IEmpresaService _empresaService;
        public EmpresaAppService(IEmpresaService empresaService)
            : base(empresaService)
        {
            _empresaService = empresaService;
        }
    }
}

﻿using FY.Retaguarda.Domain.Entities;

namespace FY.Retaguarda.Application.Interfaces
{
    public interface IHistoricoPontuacaoAppService : IAppServiceBase<HistoricoPontuacao>
    {
    }
}

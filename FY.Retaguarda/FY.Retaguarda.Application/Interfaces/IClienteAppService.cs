﻿using FY.Retaguarda.Domain.Entities;
using System.Collections.Generic;

namespace FY.Retaguarda.Application.Interfaces
{
    public interface IClienteAppService : IAppServiceBase<Cliente>
    {
        //IEnumerable<Cliente> BuscarClientesPorEmpresa(int empresaId);
    }
}

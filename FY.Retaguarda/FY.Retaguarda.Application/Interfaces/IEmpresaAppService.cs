﻿using FY.Retaguarda.Domain.Entities;

namespace FY.Retaguarda.Application.Interfaces
{
    public interface IEmpresaAppService : IAppServiceBase<Empresa>
    {
    }
}

﻿using FY.Retaguarda.Application.Interfaces;
using FY.Retaguarda.Domain.Entities;
using FY.Retaguarda.Domain.Interfaces.Services;

namespace FY.Retaguarda.Application
{
    public class HistoricoPontuacaoAppService : AppServiceBase<HistoricoPontuacao>, IHistoricoPontuacaoAppService
    {
        private readonly IHistoricoPontuacaoService _historicoPontuacaoService;
        public HistoricoPontuacaoAppService(IHistoricoPontuacaoService historicoPontuacaoService)
            : base(historicoPontuacaoService)
        {
            _historicoPontuacaoService = historicoPontuacaoService;
        }
    }
}

﻿using System.Collections.Generic;
using FY.Retaguarda.Application.Interfaces;
using FY.Retaguarda.Domain.Entities;
using FY.Retaguarda.Domain.Interfaces.Services;

namespace FY.Retaguarda.Application
{
    public class ClienteAppService : AppServiceBase<Cliente>, IClienteAppService
    {
        private readonly IClienteService _clienteService;
        public ClienteAppService(IClienteService clienteService)
            : base(clienteService)
        {
            _clienteService = clienteService;
        }

        public IEnumerable<Cliente> BuscarClientesPorEmpresa(int empresaId)
        {
            return _clienteService.BuscarClientesPorEmpresa(empresaId);
        }
    }
}

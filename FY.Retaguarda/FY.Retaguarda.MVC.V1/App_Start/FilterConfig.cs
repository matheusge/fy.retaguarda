﻿using System.Web;
using System.Web.Mvc;

namespace FY.Retaguarda.MVC.V1
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}

﻿using FY.Retaguarda.Domain.ObjectsValue;
using System;
using System.ComponentModel.DataAnnotations;

namespace FY.Retaguarda.MVC.V1.ViewModel
{
    public class ClienteViewModel
    {
        [Key]
        [Display(Name = "Código do Cliente")]
        public int ClienteId { get; set; }

        [Required(ErrorMessage = "Você não pode deixar este campo em branco.")]
        [MaxLength(150)]
        public string Nome { get; set; }

        [Required(ErrorMessage = "Você não pode deixar este campo em branco.")]
        [MaxLength(14)]
        public string CPF { get; set; }

        [Display(Name = "Gênero")]
        public EnumGenero Genero { get; set; }

        [Display(Name = "Data de Nascimento")]
        [Required(ErrorMessage = "Você não pode deixar este campo em branco.")]
        public DateTime DataNascimento { get; set; }

        [Required(ErrorMessage = "Você não pode deixar este campo em branco.")]
        [MaxLength(15)]
        public string Celular { get; set; }

        [Display(Name = "E-mail")]
        [Required(ErrorMessage = "Você não pode deixar este campo em branco.")]
        [MaxLength(150)]
        public string Email { get; set; }

        [Display(Name = "Data de Cadastro")]
        public DateTime DataCadastro { get; set; }

        [Display(Name = "Total de Pontos")]
        public double TotalPontos { get; set; }

        public int EmpresaId { get; set; }
    }
}
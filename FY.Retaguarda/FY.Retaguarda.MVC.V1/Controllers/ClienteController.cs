﻿using FY.Retaguarda.Application.Interfaces;
using FY.Retaguarda.Domain.Entities;
using FY.Retaguarda.MVC.V1.ViewModel;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace FY.Retaguarda.MVC.V1.Controllers
{
    //Annotation Authorize
    public class ClienteController : Controller
    {
        private readonly IClienteAppService _clienteApp;
        public ClienteController(IClienteAppService clienteApp)
        {
            _clienteApp = clienteApp;
        }

        public ActionResult Index()
        {
            try
            {
                var clientes = _clienteApp.GetAll();
                return View(clientes);
            }
            catch (Exception e)
            {
                return View(new List<Cliente>());
            }
        }

        public ActionResult Novo()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Novo(ClienteViewModel cliente)
        {
            try
            {
                if (!ModelState.IsValid)
                    return View(cliente);

                Cliente c = new Cliente(){
                    Nome = cliente.Nome,
                    CPF = cliente.CPF,
                    Genero = cliente.Genero,
                    DataNascimento = cliente.DataNascimento,
                    Celular = cliente.Celular,
                    Email = cliente.Email,
                    EmpresaId = 1
                };
                _clienteApp.Add(c);

                return RedirectToAction("Index");
            }
            catch (Exception e) {
                //Armazenar exception em log
                return View(cliente);
            }
        }

        public ActionResult Editar(int id)
        {
            try {
                ClienteViewModel cliente;
                var c = _clienteApp.GetById(id);
                if(c.ClienteId > 0)
                {
                    cliente = new ClienteViewModel()
                    {
                        ClienteId = c.ClienteId,
                        Nome = c.Nome,
                        CPF = c.CPF,
                        Genero = c.Genero,
                        DataNascimento = c.DataNascimento,
                        Celular = c.Celular,
                        Email = c.Email,
                        DataCadastro = c.DataCadastro,
                        TotalPontos = c.TotalPontos,
                        EmpresaId = c.EmpresaId
                    };
                    return View(cliente);
                }
                return RedirectToAction("Index");
            }
            catch(Exception e) {
                //Armazenar log de erro
                return RedirectToAction("Index");
            }
        }

        [HttpPost]
        public ActionResult Editar(ClienteViewModel cliente)
        {
            try
            {
                var c = _clienteApp.GetById(cliente.ClienteId);
                if (c.ClienteId > 0)
                {
                    c.Nome = cliente.Nome;
                    c.CPF = cliente.CPF;
                    c.Genero = cliente.Genero;
                    c.DataNascimento = cliente.DataNascimento;
                    c.Celular = cliente.Celular;
                    c.Email = cliente.Email;
                    c.TotalPontos = cliente.TotalPontos;

                    _clienteApp.Update(c);
                }
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                //Armazenar log de erro
                return RedirectToAction("Index");
            }
        }


        //public ActionResult Manutencao(int? id)
        //{
        //    try
        //    {
        //        ViewBag.TitleManutencao = id != null ? "Editando cliente" : "Novo cliente";
        //        Cliente c = new Cliente();
        //        if (id != null)
        //        {
        //            c = _clienteApp.GetById((int)id);
        //        }
        //        return View(c);
        //    }catch(Exception e)
        //    {
        //        return View(new Cliente());
        //    }
        //}

        //[HttpPost]
        //public ActionResult Manutencao(Cliente c)
        //{
        //    try
        //    {
        //        Cliente cliente = new Cliente();
        //        cliente.PreencherDados(c);
        //        cliente.EmpresaId = 1;
        //        if (cliente.ClienteId > 0){
        //            cliente = _clienteApp.Update(cliente);
        //        }
        //        else{
        //            cliente = _clienteApp.Add(cliente);
        //        }
        //        cliente = _clienteApp.Add(cliente);
        //        return View(cliente);
        //    }
        //    catch(Exception e)
        //    {
        //        return View(c);
        //    }
        //}



    }
}
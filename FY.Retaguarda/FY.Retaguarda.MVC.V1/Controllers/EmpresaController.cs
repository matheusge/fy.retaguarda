﻿using System.Web.Mvc;

namespace FY.Retaguarda.MVC.V1.Controllers
{
    //Annotation Authorize
    public class EmpresaController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Manutencao(int? id)
        {
            ViewBag.TitleManutencao = id != null ? "Editando empresa" : "Nova empresa";
            return View();
        }

        public ActionResult MinhasConfiguracoes()
        {
            //Validar identity para pegar código da empresa
            return Redirect("Manutencao/0");
        }
    }
}